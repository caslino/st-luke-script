/*
 * Name: avaamo.js
 * Description: This script will load the Chatbot in MyChart desktop.
 * History:
 */
/*/////////////////////////////////////////////////////////////////////////////////
 * Environment URL Configuration
 */ ///////////////////////////////////////////////////////////////////////////////
var pocAvaamoChannelID = 'https://c10.avaamo.com/web_channels/4698ec6e-31b3-42f0-898b-c00191ba5ee1?theme=avm-messenger';
var tstAvaamoChannelID = 'https://c10.avaamo.com/web_channels/4698ec6e-31b3-42f0-898b-c00191ba5ee1?theme=avm-messenger';
var prdAvaamoChannelID = 'https://h1.avaamo.com/web_channels/155c113d-ffed-4231-8fee-eb57abad3ad5?theme=avm-messenger';
/*/////////////////////////////////////////////////////////////////////////////////
 * Kill Switch - Set to TRUE to keep running script. 
 */ ///////////////////////////////////////////////////////////////////////////////
var ChatBotOn = true;
var PA = false;

var PocURL = "";
var TstURL = "https://tstmepicmc1v.saint-lukes.org/MyChartTST/Authentication/Login";
var ProdURL = "https://mysaintlukes.corp.saint-lukes.org/MyChartPRD/Authentication/Login";

/*/////////////////////////////////////////////////////////////////////////////////
 * Script begin. 
 */ ///////////////////////////////////////////////////////////////////////////////

// console.log("URL="+window.location.href.toLowerCase());

if (ChatBotOn === true) {
    $(document).ready(function () {
        setTimeout(function () {
            if ((window.location.href.toLowerCase().indexOf(TstURL.toLowerCase()) != -1) || (window.location.href.toLowerCase().indexOf(ProdURL.toLowerCase()) != -1)) {
                console.log("URL inside true="+window.location.href.toLowerCase());
                launchBot(PA);
                setInterval(function () {
                    if ($("iframe[name='avaamoIframe']").length === 0) {
                        launchBot(PA);
                    }
                }, 270000);
            }
        }, 1000);

    });
}

function launchBot(launchPA) {
    setTimeout(function () {
        $('script[src*="avaamo"]').each(function (i) {
            this.remove();
        });

        var baseUrl = document.location.href.toLowerCase(); //Get domain.
        
        console.log("Base URL="+baseUrl);
        console.log("Tst URL="+TstURL);
        console.log("baseURL = TstURL="+ baseUrl == TstURL);

        var AvaamoChannelID = '';

        
        if (baseUrl.indexOf(PocURL.toLowerCase()) != -1 ) //POC
        {
            AvaamoChannelID = pocAvaamoChannelID;
        } else if (baseUrl.indexOf(TstURL.toLowerCase()) != -1 ) //TST
        {
            AvaamoChannelID = tstAvaamoChannelID;
        } else if (baseUrl.indexOf(ProdURL.toLowerCase()) != -1 ) //PRD
        {
            AvaamoChannelID = prdAvaamoChannelID;
        }
        console.log("AvaamoChannelID="+AvaamoChannelID);
        if (AvaamoChannelID != '') {
            var botURL = AvaamoChannelID;
            //piece of code that launches bot
            var AvaamoChatBot = function (t) {
                function o(t, o) {
                    var n = document.createElement("script");
                    n.setAttribute("src", t),
                    n.setAttribute("id", "avm-web-channel"), 
                    n.onload = o, 
                    document.body.appendChild(n)
                }
                return this.options = t || {}, this.load = function (t) {
                    o(this.options.url, function () {
                        window.Avaamo.addFrame(), t && "function" == typeof (t) && t(window.Avaamo)
                    })
                }, this
            };
            var chatBox = new AvaamoChatBot({
                url: botURL
            });
            chatBox.load();
        }
    }, 500);
}
